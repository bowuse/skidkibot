from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import dbworker
from pyvirtualdisplay import Display
from botskidok import send_mess_admin


"""
This script parses categories of "silpo.ua/offers" and saves all items available on sale in database named silpo.db
Ich category to different table

Каждое утро запускается этот скрипт и обновляется БД.
"""


URL_ALL = 'https://silpo.ua/offers'

URL_NON_FOOD_GOODS = 'https://silpo.ua/offers?categoryId=14' 	#всякая херь, не юзается
URL_FRUIT_VEGETABLES = 'https://silpo.ua/offers?categoryId=13'
URL_MEAT = 'https://silpo.ua/offers?categoryId=1'
URL_DRINKS = 'https://silpo.ua/offers?categoryId=2'
URL_DAIRY = 'https://silpo.ua/offers?categoryId=3'				#milk products
URL_FISH = 'https://silpo.ua/offers?categoryId=4'
URL_COOKERY = 'https://silpo.ua/offers?categoryId=5'
URL_GASTRONOMY = 'https://silpo.ua/offers?categoryId=6'			#sausages
URL_CONFECTIONERY = 'https://silpo.ua/offers?categoryId=8'		#sweets
URL_BAKERY = 'https://silpo.ua/offers?categoryId=9'
URL_GROCERY = 'https://silpo.ua/offers?categoryId=11'			#бакалия (консервы и дичь)
URL_ALCOHOL = 'https://silpo.ua/offers?categoryId=12'
URL_LIST = (URL_FRUIT_VEGETABLES, URL_MEAT, URL_DRINKS, URL_DAIRY, URL_FISH, URL_COOKERY, 
				URL_GASTRONOMY, URL_CONFECTIONERY, URL_BAKERY, URL_GROCERY, URL_ALCOHOL)
TABLE_NAMES = ('FRUIT_VEGETABLES', 'MEAT', 'DRINKS', 'DAIRY', 'FISH', 'COOKERY', 'GASTRONOMY', 'CONFECTIONERY', 'BAKERY', 'GROCERY', 'ALCOHOL')



def get_items_of(driver,url):
	"""
	Returns:
		List of <li> objects or None
	
	Objects of returned list are FirefoxWebElement type
	
	Example:
		list_of_li[0].text == '37\n99\n44.57\nСосиски ТМ «М'ясна Гільдія»\n285 г'
	"""
	driver.get(url)

	try:
		ul = WebDriverWait(driver, 2).until(
       		EC.presence_of_element_located((By.XPATH, "//ul[@class='product-list product-list__per-row-2']")))
	except TimeoutException as ex:
		print("Nothing in {}".format(url))
		return None

	footer = WebDriverWait(driver, 3).until(
       	EC.presence_of_element_located((By.XPATH, "//div[@class='footer l-footer']")))

	lis = ul.find_elements_by_tag_name("li")
	prev_lis_len = 0
	new_lis_len = len(lis)

	while prev_lis_len < new_lis_len:
		footer.location_once_scrolled_into_view
		footer.location_once_scrolled_into_view
		footer.location_once_scrolled_into_view
		footer.location_once_scrolled_into_view
		lis = ul.find_elements_by_tag_name("li")
		prev_lis_len = new_lis_len
		new_lis_len = len(lis)
	
	return lis


def format_item_txt(item):
	"""
	Func formats text of FirefoxWebElement 'item'
	Returns: Tuple
	"""
	try:
		text = item.text.split("\n")
		old_price = text[2]
		new_price = "{}.{}".format(text[0], text[1])
		name = text[3]
	except Exception as e:
		print(item)
		print(e)
	return (name, new_price, old_price)


def save_to_db(items, table_name):
	db, c = dbworker.connect(dbworker.SILPO_FILE)
	dbworker.update(db, c, table_name)
			
	for item in items:
		txt = format_item_txt(item) 				#('item_name', 'new_price', 'old_price')
		if txt:
			id = items.index(item)
			dbworker.add_new_item(
				db=db, c=c, table_name=table_name, id=id, item=txt[0], new_price=txt[1], old_price=txt[2])
	dbworker.total_rows(c, table_name, True)
	dbworker.close(db)



def main():

	try:
		#Starting virtual display
		display = Display(visible=0, size=(1024, 768)) 
		display.start() 
			
		driver = webdriver.Firefox()
			
		for i in range(len(URL_LIST)):
			items = get_items_of(driver, URL_LIST[i])
			table_name = TABLE_NAMES[i]

			if items:
				save_to_db(items, table_name)

		driver.close()
		display.stop()

		send_mess_admin("💾 <code>silpo.db</code> успішно оновленна")
	except Exception as e:
		print(e)
		send_mess_admin("👺 При оновленні <code>silpo.db</code>:\n{}".format(e))



	
if __name__ == '__main__':
	main()