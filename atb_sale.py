import requests
from bs4 import BeautifulSoup
import dbworker
from botskidok import send_mess_admin

"""
This script parses "atbmarket.com/hot/akcii/economy" and saves all items available on sale in database named atb.db

Каждое утро запускается этот скрипт и обновляется БД.
"""

URL = 'https://www.atbmarket.com/hot/akcii/economy/'


def rm_whitespaces(txt):
	words = txt.split()
	return " ".join(words)


def format_price(price):
	price = price.split()[0]
	price = (price[:-2], price[-2:])
	return ".".join(price)



def save_to_db(products):
	#Save all parsed info to database
	tn = 'items'
	db , c = dbworker.connect(dbworker.ATB_FILE)
	dbworker.update(db, c, tn)
	for i in products:
		id = products.index(i)
		dbworker.add_new_item(
			db=db, c=c, table_name=tn, id=id, item=i[0], new_price=i[1],  old_price=i[2])
	dbworker.total_rows(c, tn, print_out=True)
	dbworker.close(db)


def parsing(URL):
	"""
	Returns list of <li> which contain item info
	"""
	html = requests.get(URL).text
	soup = BeautifulSoup(html, 'lxml')
	main = soup.find('section', class_='main_content_block')
	div = main.find('div', class_='container').find('div', class_='list_wrapper').find('div')
	lis = div.find_all('li')
	return lis


def purification(lis):
	"""
	Clean <li> from unuseful info
	Return products:
		Products is a list with all items on sale
		Example of a list:
			[....,
			('item_name', 'new_price', 'old_price'),
			....]
	"""
	products = []
	
	for item in lis:
		info = item.find('div', class_='promo_info')
		text = info.find('span', class_='promo_info_text').text
		item_name = rm_whitespaces(text)

		try:
			economy = item.find('div', 'economy_price_container').find('span').text
		except AttributeError:
			economy = 'Щоденна економія'

		price = format_price(item.find('div', 'promo_price').text)
		old_price = item.find('span', class_ = 'promo_old_price').text
		products.append((item_name, price, old_price))
	return products
	


def main():
	try:
		lis = parsing(URL)				#list of <li>
		products = purification(lis)	#list containing item_name and price
		save_to_db(products)			#cleaning and updating database
		send_mess_admin("💾 <code>atb.db</code> оновленна успішно")
	except Exception as e:
		send_mess_admin("👺 При оновленні <code>atb.db</code>:\n{}".format(e))
		raise e


if __name__ == "__main__":
	main()

