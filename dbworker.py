import sqlite3
from sqlite3 import Error


ATB_FILE = '/home/cookie/Sales/databases/atb.db'
SILPO_FILE = '/home/cookie/Sales/databases/silpo.db'
USERS_FILE = '/home/cookie/Sales/databases/users.db'


def connect(file):#sqlite3.Connection, sqlite3.Cursor
	"""
	Connect to database
	"""
	db = sqlite3.connect(file)
	c = db.cursor()
	return db, c


def add_user(id, username):
	with sqlite3.connect(USERS_FILE) as db:
		try:
			cursor = db.cursor()
			state = "start"
			cursor.execute("""INSERT INTO user(id, username, state) 
				VALUES(?, ?, ?)""",(id, username, state))
			db.commit()
			return True
		except Exception as e:
			print("new_user Error:", e)
			db.rollback()
			return False


def add_user_state(id, state):
	with sqlite3.connect(USERS_FILE) as db:
		try:
			cursor = db.cursor()
			cursor.execute("""UPDATE user SET state = ? WHERE id = ?""", (state, id))
			db.commit()
			return True
		except Exception as e:
			print("add_user_state Error:", e)
			db.rollback()
			return False


def get_user_state(id):
	with sqlite3.connect(USERS_FILE) as db:
		try:
			c = db.cursor()
			c.execute("""SELECT state FROM user WHERE id = {id}""".format(id=id))
			state = c.fetchone()
			return state[0]
		except Exception as e:
			print("get_user_state Exception occured:", e)
			return "start"		#first state



def update(db, c, table_name):
	"""
	Drop old table table_name and creates new one
	"""
	try:
		c.execute('''DROP TABLE {tn}'''.format(tn=table_name))
		print("\nTable {} was droped".format(table_name))
	except Error as e:
		print(f"Error in update(): {e}")
		db.rollback()

	try:
		c.execute('''
	    	CREATE TABLE {tn} (
	    	id INTEGER, item_name TEXT PRIMARY KEY, new_price TEXT, old_price TEXT)
		'''.format(tn=table_name))
		db.commit()
		print("New table {} created and commited".format(table_name))
	except Error as e:
		print("Error in update(): {}".format(e))
		db.rollback()


def add_new_item(db, c, table_name, id, item, new_price, old_price):
	"""
	Add a new item to table table_name and commits
	"""
	try:
		c.execute("""
			INSERT INTO {tn} (id, item_name, new_price, old_price)
			VALUES(?,?,?,?)""".format(tn=table_name), 
				(id, item, new_price, old_price))
		db.commit()
	except Error as e:
		print(f"Error in add_new_item() to {table_name}: {e}")
		db.rollback()


def total_rows(c, table_name, print_out=False):
	"""
	Returns the total number of rows in the table_name
	"""
	c.execute('SELECT COUNT(*) FROM {tn}'.format(tn=table_name))
	count = c.fetchall()
	if print_out:
		print('Total rows in {}: {}'.format(table_name, count[0][0]))
	return count[0][0]


def get_all_items(c, table_name):
	"""
	Returns all items that are contained in table_name
	"""
	c.execute("""SELECT item_name, new_price, old_price FROM {tn}""".format(tn=table_name))
	all_items = c.fetchall()
	return all_items


def get_items_from_to(c, table_name, f, t):#List
	"""
		Returns items from f to t-1
		item is [id, item_name, new_price, old_price]
	"""
	limit = t-f
	c.execute("""
		SELECT item_name, new_price, old_price FROM {tn} LIMIT {l} OFFSET {f}""".format(tn=table_name, l=limit, f=f))
	items = c.fetchall()
	return items


def search_items_by(c, table_name, key_word):#List
	"""
		Search by key_word
	"""
	word_low = "%" + key_word.lower() + "%"
	word_cap = "%" + key_word.capitalize() + "%"
	items = []
	#search for word_cap
	c.execute("""
		SELECT item_name, new_price, old_price 
		FROM {tn}
		WHERE item_name
		LIKE ?""".format(
			tn = table_name), (word_cap,))
	fetched = c.fetchall()
	if len(fetched) > 0:
		for i in fetched:
			items.append(i)

	#search for word_low
	c.execute("""
		SELECT item_name, new_price, old_price
		FROM {tn}
		WHERE item_name
		LIKE ?""".format(
			tn = table_name), (word_low,))
	fetched = c.fetchall()
	if len(fetched) > 0:
		for i in fetched:
			items.append(i)
	return items


def close(db):
	db.close()


def main():
	pass


if __name__ == "__main__":
	main()

