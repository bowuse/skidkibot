import dbworker as dbw
import telebot
import cherrypy
import keyboard as kb
from time import sleep
import logging

logging.basicConfig(
    filename='log.log',
    format = '[%(asctime)s] %(levelname)s: %(message)s',
    level = logging.INFO,
    datefmt = '%d/%m/%Y %I:%M:%S %p')


API_TOKEN = "783546117:AAE3TN64CJv2WDHthQcZU666xtGGgIAvEVs"		#prodskidkibot
bot = telebot.TeleBot(API_TOKEN)


ADMIN_ID = '238032336'
ITEMS_IN_PAGE = 4


SILPO_TABLE_NAMES = ('FRUIT_VEGETABLES', 'MEAT', 'DRINKS', 'DAIRY', 'FISH', 'COOKERY', 'GASTRONOMY', 'CONFECTIONERY', 'BAKERY', 'GROCERY', 'ALCOHOL')
category_nameSLP = "{}\n".format(kb.SILPO_btn.text)		# + SILPO_btns[i].text


class ATB:
	#Main part of message with items
	@staticmethod
	def items_blank(items) -> str:
		blank = [""]
		for i in items:
			txt = "🔹 {}\n✅ Нова ціна - <code>{}</code> грн. \n➖ Стара ціна - <code>{}</code> грн.\n".format(i[0], i[1], i[2])
			blank.append(txt)
		return "\n".join(blank)

	@staticmethod
	def get_items(f, t) -> str:
		"""
			Items from #f to #t-1
			Returns text that is send to user
		"""
		db, c = dbw.connect(dbw.ATB_FILE)
		items = dbw.get_items_from_to(c, 'items', f, t)
		dbw.close(db)
		return ATB.items_blank(items)

	@staticmethod
	def get_all_items() -> list:
		"""
			Returns items saved in ATB database
		"""
		table_name = "items"
		db, c = dbw.connect(dbw.ATB_FILE)
		items = dbw.get_all_items(c, table_name)
		dbw.close(db)
		return items

	@staticmethod
	def update_state(id, cur_page):
		state = "ATB {}".format(cur_page)
		dbw.add_user_state(id, state)

	@staticmethod
	def get_cur_page(id):
		try:
			page = int(dbw.get_user_state(id).split()[1])
			return page
		except Exception as e:
			print(e)
			return 0

items_all = len(ATB.get_all_items())
num_of_pagesATB = (items_all+ITEMS_IN_PAGE-1) // ITEMS_IN_PAGE

class SLP:
	@staticmethod
	def items_blank(items) -> str:
		blank = [""]
		for i in items:
			txt = "🔸 {}\n✅ Нова ціна - <code>{}</code> грн.\n➖ Стара ціна - <code>{}</code> грн.\n".format(i[0], i[1], i[2])
			blank.append(txt)
		return "\n".join(blank)

	@staticmethod
	def get_all_items(table_name) -> list:
		"""
			Returns all items saved in table_name in silpo.db 
		"""
		db, c = dbw.connect(dbw.SILPO_FILE)
		items = dbw.get_all_items(c, table_name)
		dbw.close(db)
		return items

	@staticmethod
	def get_page(table_name, f, t) -> str:
		db, c = dbw.connect(dbw.SILPO_FILE)
		items = dbw.get_items_from_to(c, table_name, f, t)
		dbw.close(db)
		return SLP.items_blank(items)

	@staticmethod
	def update_state(id, category, page):
		state = "{} {}".format(category, page)
		dbw.add_user_state(id, state)


def send_mess_admin(text):
	bot.send_message(ADMIN_ID, text, parse_mode="HTML")
	logging.info("Message send to admin \'{}\'".format(text))


def send_message(id, text, markup=None, pm="HTML"):
	bot.send_message(
		chat_id = id,
		text = text,
		reply_markup = markup,
		parse_mode = pm)


def edit_message(mess, chat, text, markup=None, pm="HTML"):
	bot.edit_message_text(
		message_id = mess,
		chat_id = chat,
		text = text,
		reply_markup = markup,
		parse_mode = pm)



#Message with page
def page_blank(name, cur_page, num_of_pages, text) -> str:
	return "{}\n{}<code>ст.{}/{}</code>".format(name, text, cur_page+1, num_of_pages)



#User choose SILPO or ATB
@bot.message_handler(commands = ['start', 'help'])
def welcome(m):
	bot.send_message(m.chat.id, "🤚 Вітаю!\nЯ покажу тобі перелік акційних товарів у мережах\n🔶 Сільпо та 🔷 АТБ\n🛒 Обери супермаркет, або скористайся пошуком", reply_markup = kb.market_kb)
	if dbw.add_user(m.chat.id, m.from_user.username):
		logging.info("New user_id added {}".format(m.chat.id))



#Send ATB items 
@bot.message_handler(func = lambda m: m.text == kb.ATB_btn.text)
def ATB_items(m):
	global num_of_pagesATB

	cur_page = 0
	ATB.update_state(m.chat.id, cur_page)

	txt = ATB.get_items(f=0,t=ITEMS_IN_PAGE)		#first 4 items from dbw
	
	send_message(id = m.chat.id,
				text = page_blank(m.text, cur_page, num_of_pagesATB, txt),
				markup = kb.ATB_firstpage_kb)

	logging.info("ATB firstpage send to {}".format(m.chat.id))


#Answer inline calls ATB
@bot.callback_query_handler(func=lambda call: 'ATB' in call.data)
def answer_ATB(call):
	global num_of_pagesATB
	try:
		cur_page = ATB.get_cur_page(call.message.chat.id)
	except Exception as e:
		send_message(id = call.message.chat.id,
				text = page_blank(
					kb.ATB_btn.text,
					0,
					num_of_pagesATB,
					ATB.get_items(f=0,t=ITEMS_IN_PAGE)),
				markup = kb.ATB_firstpage_kb)

	if call.data == 'next_ATB':
		cur_page += 1
		ATB.update_state(call.message.chat.id, cur_page)

		text = ATB.get_items(
			ITEMS_IN_PAGE * cur_page, ITEMS_IN_PAGE * (cur_page+1))



		if cur_page >= num_of_pagesATB - 1:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
			 	text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
			 	markup = kb.ATB_lastpage_kb)
		elif cur_page <= 0:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
				text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
				markup = kb.ATB_firstpage_kb)
		else:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
				text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
				markup = kb.ATB_inln_kb)
		logging.info("ATB next_page send to {}".format(call.message.chat.id))
			

	if call.data == 'prev_ATB':
		cur_page -= 1
		ATB.update_state(call.message.chat.id, cur_page)

		text = ATB.get_items(
			ITEMS_IN_PAGE * cur_page, ITEMS_IN_PAGE * (cur_page+1))
		if cur_page <= 0:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
				text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
				markup = kb.ATB_firstpage_kb)
		elif cur_page >= num_of_pagesATB - 1:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
			 	text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
			 	markup = kb.ATB_lastpage_kb,)
		else:
			edit_message(
				mess = call.message.message_id,
				chat = call.message.chat.id,
				text = page_blank(kb.ATB_btn.text, cur_page, num_of_pagesATB, text),
				markup = kb.ATB_inln_kb)
		logging.info("ATB prev_page send to {}".format(call.message.chat.id))


#User choose category SILPO
@bot.message_handler(func = lambda m: m.text == kb.SILPO_btn.text)
def SILPO_category(m):
	send_message(
		id = m.chat.id,
		text = "Обери категорію продуктів:",
		markup = kb.SILPO_category_kb)
	logging.info("Silpo send to {}".format(m.chat.id))


#Next SLP
@bot.callback_query_handler(func = lambda call: call.data == "next_SLP")
def next_SLP(call):
	try:
		cur_category, cur_page = dbw.get_user_state(call.message.chat.id).split()
		cur_page = int(cur_page) + 1
	except Exception as e:
		send_message(
			id = call.message.chat.id,
			text = "Обери категорію продуктів:",
			markup = kb.SILPO_category_kb)
		logging.info("Exception next_SLP: Silpo_category send to {}".format(m.chat.id))

	SLP.update_state(call.message.chat.id, cur_category, cur_page)

	category_name = kb.SILPO_btn.text
	for i in range(len(SILPO_TABLE_NAMES)):
		if cur_category == SILPO_TABLE_NAMES[i]:
			category_name = "{}\n{}".format(kb.SILPO_btn.text, kb.SILPO_btns[i].text)			

	items_in_cat = len(SLP.get_all_items(cur_category))
	num_of_pages = (items_in_cat + ITEMS_IN_PAGE-1) // ITEMS_IN_PAGE
	
	txt = SLP.get_page(
		cur_category,
		cur_page * ITEMS_IN_PAGE,
		(cur_page+1) * ITEMS_IN_PAGE)

	if cur_page >= num_of_pages - 1:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_lastpage_kb)
	elif cur_page <= 0:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_firstpage_k)
	else:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_inln_kb)
	logging.info("Silpo next_page send to {}".format(call.message.chat.id))


#Previous SLP
@bot.callback_query_handler(func = lambda call: call.data == "prev_SLP")
def prev_SLP(call):
	try:
		cur_category, cur_page = dbw.get_user_state(call.message.chat.id).split()
		cur_page = int(cur_page) - 1
	except Exception as e:
		send_message(
			id = call.message.chat.id,
			text = "Обери категорію продуктів:",
			markup = kb.SILPO_category_kb)
		logging.info("Exception prev_SLP: Silpo_category send to {}".format(m.chat.id))

	SLP.update_state(call.message.chat.id, cur_category, cur_page)

	category_name = kb.SILPO_btn.text
	for i in range(len(SILPO_TABLE_NAMES)):
		if cur_category == SILPO_TABLE_NAMES[i]:
			category_name = "{}\n{}".format(kb.SILPO_btn.text, kb.SILPO_btns[i].text)			

	items_in_cat = len(SLP.get_all_items(cur_category))
	num_of_pages = (items_in_cat + ITEMS_IN_PAGE-1) // ITEMS_IN_PAGE

	txt = SLP.get_page(
		cur_category,
		cur_page * ITEMS_IN_PAGE,
		(cur_page + 1) * ITEMS_IN_PAGE)
	if cur_page <= 0:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_firstpage_kb)
	elif cur_page >= num_of_pages - 1:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_lastpage_kb)
	else:
		edit_message(
			mess = call.message.message_id,
			chat = call.message.chat.id,
			text = page_blank(category_name, cur_page, num_of_pages, txt),
			markup = kb.SILPO_inln_kb)
	logging.info("Silpo prev_page send to {}".format(call.message.chat.id))

#Choose category
@bot.callback_query_handler(func = lambda call: 'SLP' in call.data)
def answer_SLP(call):
	cur_page = 0

	for i in range(len(kb.SILPO_categorys_calldata)):
		if call.data == kb.SILPO_categorys_calldata[i]:
			category_name = "{}\n{}".format(kb.SILPO_btn.text, kb.SILPO_btns[i].text)

			cur_category = SILPO_TABLE_NAMES[i]
			state = "{} {}".format(cur_category, cur_page)
			dbw.add_user_state(call.message.chat.id, state)

			items_in_cat = len(SLP.get_all_items(cur_category))
			num_of_pages = (items_in_cat + ITEMS_IN_PAGE-1) // ITEMS_IN_PAGE

			txt = SLP.get_page(cur_category, 0, ITEMS_IN_PAGE)
			if items_in_cat == 0:
				send_message(
					call.message.chat.id,
					"🙄 На жаль, нічого немає")
			elif items_in_cat <= ITEMS_IN_PAGE:
				send_message(
					call.message.chat.id,
					text = page_blank(category_name, cur_page, num_of_pages, txt))
			else:
				send_message(
					call.message.chat.id,
					text = page_blank(category_name, cur_page, num_of_pages, txt),
					markup = kb.SILPO_firstpage_kb)
				logging.info("Silpo {} choosed {}".format(cur_category, call.message.chat.id))




#Search button pressed
@bot.message_handler(func = lambda m: m.text == kb.search_btn.text)
def get_ready_for_search(m):
	msg = bot.send_message(m.chat.id, "Відправ назву потрібного тобі товару та я швидко його знайду")
	bot.register_next_step_handler(msg, searching)
	logging.info("Search started {}".format(m.chat.id))


#Searching for word and sending found items
def searching(m):
	word = m.text
	itemsSLP = []
	itemsATB = []
	silpodb, c = dbw.connect(dbw.SILPO_FILE)
	for table in SILPO_TABLE_NAMES:
		t = dbw.search_items_by(c, table, word)
		if len(t) != 0:
			for i in t:
				itemsSLP.append(i)
	dbw.close(silpodb)

	atbdb, c = dbw.connect(dbw.ATB_FILE)
	t = dbw.search_items_by(c, "items", word)
	if len(t) != 0:
		for i in t:
			itemsATB.append(i)
	dbw.close(atbdb)
	
	try:
		if len(itemsSLP) > 0:
			txt = SLP.items_blank(itemsSLP)
			send_message(
				id = m.chat.id,
				text = "{}\n{}".format(kb.SILPO_btn.text, txt))
		if len(itemsATB) > 0:
			txt = ATB.items_blank(itemsATB)
			send_message(
				id = m.chat.id,
				text = "{}\n{}".format(kb.ATB_btn.text, txt))
		if len(itemsSLP) == len(itemsATB) == 0:
			send_message(
				id = m.chat.id,
				text = "🙄 На жаль, на даний товар немає знижок")
		logging.info("Search end {}".format(m.chat.id))
	except Exception as e:
		print(e)
		send_message(
			id = m.chat.id,
			text = "Вибач, не зрозумів тебе")
		logging.info("Search crash {}, {}".format(m.chat.id, e))
		get_ready_for_search(m)




class WebhookServer(object):
    # index равнозначно /, т.к. отсутствию части после ip-адреса (грубо говоря)
    @cherrypy.expose
    def index(self):
        length = int(cherrypy.request.headers['content-length'])
        json_string = cherrypy.request.body.read(length).decode("utf-8")
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        sleep(1)


if __name__ == '__main__':
    cherrypy.config.update({
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 7774,
        'engine.autoreload.on': False
    })
    cherrypy.quickstart(WebhookServer(), '/', {'/': {}})