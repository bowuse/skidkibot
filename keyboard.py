from telebot import types

#First keyboard
SILPO_btn = types.KeyboardButton("🔶 Сільпо")
ATB_btn = types.KeyboardButton("🔷 АТБ")
search_btn = types.KeyboardButton("🔍 Пошук")
market_kb = types.ReplyKeyboardMarkup(resize_keyboard=True)
market_kb.row(SILPO_btn, ATB_btn)
market_kb.row(search_btn)

#Keyboard SILPO category choose
fruit_veget_btn = types.InlineKeyboardButton("🍏 Овочі та фрукти", callback_data='fruitSLP')
meat_btn = types.InlineKeyboardButton("🥩 Мʼясо", callback_data='meatSLP') 
drinks_btn = types.InlineKeyboardButton("🥤 Напої", callback_data='drinkSLP')
dairy_btn = types.InlineKeyboardButton("🥛 Молочні продукти", callback_data='dairySLP')
fish_btn = types.InlineKeyboardButton("🐟 Риба", callback_data='fishSLP')
cookery_btn = types.InlineKeyboardButton("🥗 Кулінарія", callback_data='cookerySLP')
gastro_btn = types.InlineKeyboardButton("🧀 Гастрономія", callback_data='gastroSLP')
confection_btn = types.InlineKeyboardButton("🍬 Кондитерські вироби", callback_data='confectionSLP')
bakery_btn = types.InlineKeyboardButton("🥐 Випічка", callback_data='bakerySLP')
grocery_btn = types.InlineKeyboardButton("🥫 Бакалія", callback_data='grocerySLP')
alcohol_btn = types.InlineKeyboardButton("🍻 Алкоголь", callback_data='alcoSLP')
SILPO_category_kb = types.InlineKeyboardMarkup(row_width=2)

SILPO_category_kb.add(fruit_veget_btn, meat_btn, drinks_btn, dairy_btn, fish_btn,
	cookery_btn, gastro_btn, confection_btn, bakery_btn, grocery_btn, alcohol_btn)

SILPO_btns = (fruit_veget_btn, meat_btn, drinks_btn, dairy_btn, fish_btn,
	cookery_btn, gastro_btn, confection_btn, bakery_btn, grocery_btn, alcohol_btn)
SILPO_categorys_calldata = [btn.callback_data for btn in SILPO_btns]							#List of callback_data buttons


#Inline kb to switch between ATB items
nextATB_ibtn = types.InlineKeyboardButton("\u27a1", callback_data='next_ATB')
prevATB_ibtn = types.InlineKeyboardButton("\u2b05", callback_data='prev_ATB')
ATB_inln_kb = types.InlineKeyboardMarkup()
ATB_inln_kb.add(prevATB_ibtn, nextATB_ibtn)
ATB_firstpage_kb = types.InlineKeyboardMarkup()
ATB_firstpage_kb.add(nextATB_ibtn)
ATB_lastpage_kb = types.InlineKeyboardMarkup()
ATB_lastpage_kb.add(prevATB_ibtn)


#Inline kb to switch between SILPO items
nextSILPO_ibtn = types.InlineKeyboardButton("\u27a1", callback_data='next_SLP')
prevSILPO_ibtn = types.InlineKeyboardButton("\u2b05", callback_data='prev_SLP')
SILPO_inln_kb = types.InlineKeyboardMarkup()
SILPO_inln_kb.add(prevSILPO_ibtn, nextSILPO_ibtn)
SILPO_firstpage_kb = types.InlineKeyboardMarkup()
SILPO_firstpage_kb.add(nextSILPO_ibtn)
SILPO_lastpage_kb = types.InlineKeyboardMarkup()
SILPO_lastpage_kb.add(prevSILPO_ibtn)